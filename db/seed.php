<?php

if ( empty( $app ) ) {
    require __DIR__ . '/../app/app.php';
}

//$app->run();

$user = \MyApp\Models\User::find(1);
if ( !empty( $user ) ) {
  die("Data already installed.\n");
}

// **************************************************
// Groups
// **************************************************

echo "Create Groups...\n";

try {

    $group = Sentry::createGroup(array(
        'name' => 'Administradores',
        'permissions' => array(
            'admin' => 1,
            'users' => 1,
        ),
    ));

} catch (Cartalyst\Sentry\Groups\NameRequiredException $e) {
    echo 'Name field is required';
} catch (Cartalyst\Sentry\Groups\GroupExistsException $e) {
    echo 'Group Administradores already exists';
}

try {

    $group = Sentry::createGroup(array(
        'name' => 'Usuarios',
        'permissions' => array(
            'admin' => 0,
            'users' => 1,
        ),
    ));

} catch (Cartalyst\Sentry\Groups\NameRequiredException $e) {
    echo 'Name field is required';
} catch (Cartalyst\Sentry\Groups\GroupExistsException $e) {
    echo 'Group Usuarios already exists';
}

// **************************************************
// Create Admin User
// **************************************************

echo "Create Admin User...\n";

try {

    $user = Sentry::createUser(array(
        'email' => 'admin@elitegol.eu',
        'password' => 'admin123admin',
        'activated' => true,
    ));

    // Find the group using the group id
    $adminGroup = Sentry::findGroupById(1);

    // Assign the group to the user
    $user->addGroup($adminGroup);

} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
    echo 'Login field is required.';
} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
    echo 'Password field is required.';
} catch (Cartalyst\Sentry\Users\UserExistsException $e) {
    echo 'User with this login already exists.';
} catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
    echo 'Group was not found.';
}

// **************************************************
// Seed Data
// **************************************************

echo "Seed Data...\n";

// Sports
// INSERT INTO  `elitegol`.`sports` (`id` , `name` , `slug`) VALUES (NULL ,  'Futbol',  'futbol');

// Competitions
// INSERT INTO `elitegol`.`competitions` (`id`, `name`, `slug`, `description`, `sport_id`, `status`) VALUES (NULL, 'UEFA Champions League', 'uefa-champions-league', 'UEFA Champions League', '1', '1');

// Events
// INSERT INTO `elitegol`.`events` (`id`, `title`, `slug`, `competition_id`, `description`, `published_at`, `finished_at`, `status`) VALUES (NULL, 'Real Madrid vs FC Barcelona', 'real-madrid-vs-fc-barcelona', '1', 'Real Madrid vs FC Barcelona', '2014-12-27 15:00:00', '2014-12-27 20:00:00', '1');

\MyApp\Models\Sport::create(array('name' => 'Futbol', 'slug' => 'futbol'));

\MyApp\Models\Competition::create(array(
    'name' => 'UEFA Champions League',
    'slug' => 'uefa-champions-league',
    'description' => 'UEFA Champions League',
    'sport_id' => 1,
    'status' => 1
));

\MyApp\Models\Event::create(array(
    'title' => 'Real Madrid vs FC Barcelona',
    'slug' => 'real-madrid-vs-fc-barcelona',
    'competition_id' => 1,
    'description' => 'Real Madrid vs FC Barcelona',
    'published_at' => date("Y-m-d H:i:s"),
    'finished_at' => date("Y-m-d H:i:s"),
'status' => 1

));

echo "Done!\n";
