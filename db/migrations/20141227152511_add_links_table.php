<?php

use Phpmig\Migration\Migration;

class AddLinksTable extends Migration
{
    protected $tableName;
    /* @var \Illuminate\Database\Schema\Builder $schema */
    protected $schema;

    public function init()
    {
        $this->tableName = 'links';
        $this->schema = $this->get('schema');
    }

    /**
     * Do the migration
     */
    public function up()
    {
        /* @var \Illuminate\Database\Schema\Blueprint $table */
        $this->schema->create($this->tableName, function ($table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('channel_id');
            $table->tinyInteger('status');
            // We'll need to ensure that MySQL uses the InnoDB engine to
            // support the indexes, other engines aren't affected.
            $table->engine = 'InnoDB';
            //$table->primary(array('event_id', 'channel_id'));
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $this->schema->drop($this->tableName);
    }
}
