<?php

use Phpmig\Migration\Migration;

class AddColumnsToLinks extends Migration
{
    protected $tableName;
    /* @var \Illuminate\Database\Schema\Builder $schema */
    protected $schema;

    public function init()
    {
        $this->tableName = 'links';
        $this->schema = $this->get('schema');
    }

    /**
     * Do the migration
     */
    public function up()
    {
        /* @var \Illuminate\Database\Schema\Blueprint $table */
        $this->schema->table($this->tableName, function ($table) {
            $table->char('language', 2);
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $this->schema->table($this->tableName, function ($table){
            $table->dropColumn('language');
        });
    }
}
