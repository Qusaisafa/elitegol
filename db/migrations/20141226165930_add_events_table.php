<?php

use Phpmig\Migration\Migration;

class AddEventsTable extends Migration
{

    protected $tableName;
    /* @var \Illuminate\Database\Schema\Builder $schema */
    protected $schema;

    public function init()
    {
        $this->tableName = 'events';
        $this->schema = $this->get('schema');
    }

    /**
     * Do the migration
     */
    public function up()
    {

        /* @var \Illuminate\Database\Schema\Blueprint $table */
        $this->schema->create($this->tableName, function ($table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->integer('competition_id');
            $table->string('description')->nullable();
            $table->timestamp('published_at');
            $table->timestamp('finished_at');
            $table->tinyInteger('status');
            //$table->timestamps();
            $table->engine = 'InnoDB';
            $table->index('slug');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $this->schema->drop($this->tableName);
    }
}
