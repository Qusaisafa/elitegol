function confirmar(mensaje) {
    return confirm(mensaje);
}

function abrir_evento(id) {
    var url = 'ajax/abrir_evento/' + id;
    var spinner = '#evento-cargando-' + id;
    var capa = '#evento-' + id;
    var pars = '';

    $("#enlace-evento-" + id).hide();
    $(spinner).show();
    $(capa).load(url, pars, function () {
        //$(capa).hide();
        $(capa).slideDown(200);
        $(spinner).hide();
        $("#enlace-evento-" + id).html("Ocultar enlaces");
        $("#enlace-evento-" + id).attr("href", "javascript:cerrar_evento(" + id + ")");
        $("#enlace-evento-" + id).show();
    });
}

function abrir_evento_ficha(id) {
    var url = 'ajax/abrir_evento_ficha.php';
    var spinner = '#enlaces-cargando';
    var capa = '#enlaces';
    var pars = 'id=' + id;

    $(spinner).show();
    $(capa).load(url, pars, function () {
        $(spinner).hide();
    });
}

function cerrar_evento(id) {
    var spinner = '#evento-cargando-' + id;
    var capa = '#evento-' + id;
    var pars = 'id=' + id;

    $(spinner).show();
    $(capa).slideUp(200);
    $(spinner).hide();

    $("#enlace-evento-" + id).html("Ver enlaces");
    $("#enlace-evento-" + id).attr("href", "javascript:abrir_evento(" + id + ")");
}