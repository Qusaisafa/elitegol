$(document).ready(function () {

    $(".change_language_channel").change(function () {

        var link_id = $(this).attr('data-link');
        var language = $(this).val();

        console.log(link_id);
        console.log(language);

        $.ajax({
            type: "POST",
            //url: '/elitegol/public_html/panel/channel/' + link_id,
            url: '/panel/channel/' + link_id,
            data: {
                'link_id': link_id,
                'language': language
            },
            success: function (msg) {
                console.log(msg)
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });

    });

});