$(document).ready(function () {

    $('#import_events').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true,
        "iDisplayLength": 35,
        language: {
            url: 'http://cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Spanish.json'
        }
    });

    $('#simple_table').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true,
        "iDisplayLength": 35,
        language: {
            url: 'http://cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Spanish.json'
        }
    });

});