$(document).ready(function () {

    $('#collection_selection_toggle_all').change(function () {
        var value = $('#collection_selection_toggle_all').prop("checked");
        $(':checkbox').prop('checked', value);
    });

});