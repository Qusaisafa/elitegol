<?php
namespace MyApp\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Channel extends Model
{
    protected $fillable = array(
        'id',
        'user_id',
        'title',
        'url',
        'language',
        'quality',
        'software',
        'status'
    );

    public $timestamps = false;

    public static function isValidate()
    {
        return true;
    }

    public static function getOwnChannelById($user_id, $id)
    {
        return Channel::where('user_id', '=', $user_id)
            ->where('id', '=', $id)
            ->remember(5)
            ->firstOrFail();
    }

    public static function getChannelsByUser($user_id)
    {
        return Channel::where('user_id', '=', $user_id)
            ->where('status', '=', 1)
            ->get();//->remember(5)
    }

    public static function getChannelsByEventId($id)
    {
        return Channel::leftJoin('links', function ($join) {
            $join->on('links.channel_id', '=', 'channels.id');
        })
            ->where('links.event_id', '=', $id)
            ->where('links.status', '=', 1)
            ->groupBy('links.id')
            ->get(array(
                'channels.id',
                'channels.title',
                'channels.url',
                'channels.language',
                'channels.quality',
                'channels.software',
                'channels.status',
            ));
    }

}