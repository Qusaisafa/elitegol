<?php
namespace MyApp\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Sport extends Model
{
    protected $fillable = array('id', 'name', 'slug');
    public $timestamps = false;

    public static function isValidate()
    {
        return true;
    }
}