<?php
namespace MyApp\Scrapers\LaCronicaVirtual;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class DeportesScraper extends BaseScraper
{
    private $url = 'http://www.lacronicavirtual.com/tvdeportes/deportes.html';
    private $event_duration = 2; # hours
    public $sport_id = 8;
    public $default_competition = "Rugby";

    public function getData()
    {
        $client = new Client(array(
            'HTTP_HOST' => $this->ip_host,
            'HTTP_USER_AGENT' => $this->user_agent
        ));

        $crawler = $client->request('GET', $this->url);

        $nodeValues = $crawler->filter('#polideportivo_tablas tr')->each(function (Crawler $node, $i) {

            if ($i == 0) {
                return false;
            }

            $date_row = $node->filter('td strong')->first();

            global $my_date;

            if (count($date_row) >= 1) {

                $my_date = $date_row->text();

            } else {

                $values = $node->filterXPath('//td')->each(function (Crawler $node, $i) {
                    if ($i == 3) { // Fecha y Hora
                        global $my_date;
                        $my_time = $node->text();
                        $row = BaseScraper::convert_to_datetime($my_date, $my_time, 2);
                        return $values[] = $row;
                    }
                    return $values[] = $node->text();
                });

                return $values;
            }

        });

        $nodeValues = array_values(array_filter($nodeValues));
        return $nodeValues;
    }

    public function formatData($_data)
    {
        /*

        [0] => Array
                (
                    [0] => Fútbol sala
                    [1] => Liga Nacional
                    [2] => El Pozo Murcia - FC Barcelona
                    [3] => Carbon\Carbon Object
                        (
                            [date] => 2015-05-30 12:45:00
                            [timezone_type] => 3
                            [timezone] => +02:00
                        )

                    [4] => TDP / E3
                )

        */
        $data = array();

        foreach ($_data as $item) {

            $competition = $this->_get_competition($item[0]);
	        $title = $item[2] . ' - ' . $item[1];

	        $obj = new \stdClass();
            $obj->title = $title;
            $obj->slug = $this->url_slug($item[2]);
            $obj->competition_id = $competition['id'];
            $obj->competition_name = $competition['name'];;
            $obj->description = '';
            $obj->published_at = $item[3]['published_at'];
            $obj->finished_at = $item[3]['finished_at'];
            $data[] = $obj;
        }

        return $data;
    }

}