<?php
namespace MyApp\Scrapers\LaCronicaVirtual;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class CiclismoScraper extends BaseScraper
{
    private $url = 'http://www.lacronicavirtual.com/tvdeportes/ciclismo.html';
    private $event_duration = 2; # hours
    public $sport_id = 5;
    public $default_competition = "Ciclismo";
	public $my_default_time = "14.30";

    public function getData()
    {
        $client = new Client(array(
            'HTTP_HOST' => $this->ip_host,
            'HTTP_USER_AGENT' => $this->user_agent
        ));

        $crawler = $client->request('GET', $this->url);

        $nodeValues = $crawler->filter('#ciclismo_tablas tr')->each(function (Crawler $node, $i) {

            if ($i == 0) {
                return false;
            }

            $date_row = $node->filter('td strong')->first();

            global $my_date;

            if (count($date_row) >= 1) {

                $my_date = $date_row->text();

            } else {

                $values = $node->filterXPath('//td')->each(function (Crawler $node, $i) {
                    if ($i == 2) { // Fecha y Hora
                        global $my_date;
                        $my_time = $node->text();

	                    if (strlen($my_time) != 5) { //NOTE: fixed empty hour.
		                    $my_time = $this->my_default_time;
	                    }

	                    $my_time = empty($my_time) ? : $my_time;
                        $row = BaseScraper::convert_to_datetime($my_date, $my_time, 2);
                        return $values[] = $row;
                    }
                    return $values[] = $node->text();
                });

                return $values;
            }

        });

        $nodeValues = array_values(array_filter($nodeValues));
        return $nodeValues;
    }

    public function formatData($_data)
    {
        $data = array();

        foreach ($_data as $item) {

            $competition = $this->_get_competition($item[0]);

            $obj = new \stdClass();
            $obj->title = $item[1];
            $obj->slug = $this->url_slug($item[1]);
            $obj->competition_id = $competition['id'];
            $obj->competition_name = $competition['name'];;
            $obj->description = '';
            $obj->published_at = $item[2]['published_at'];
            $obj->finished_at = $item[2]['finished_at'];
            $data[] = $obj;
        }

        return $data;
    }

	public static function get_my_time($my_time)
	{
		$time = explode('.', $my_time);
		$data = array();
		$data['hour'] = $time[0];
		$data['minute'] = $time[1];
		return $data;
	}
}