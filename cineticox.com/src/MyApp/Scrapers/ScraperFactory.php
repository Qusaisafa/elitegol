<?php
namespace MyApp\Scrapers;

/**
 * Class ScraperFactory
 * @package MyApp\Scrapers
 */
class ScraperFactory
{
    /**
     * @var string The Class Name
     */
    private $class_name;

    /**
     * @param $site string The Site to Scrap
     * @param $sport string  The Type of Sport
     */
    public function __construct($site, $sport)
    {
        $class_name = $this->getClassName($site, $sport);
        $this->class_name = new $class_name;
    }

    /**
     *
     */
    public function getAll()
    {
        return $this->class_name->process();
    }

    /**
     * Get Class Name
     *
     * @param $site The Site to Scrap
     * @param $sport The Type of Sport
     * @return string Return the full class name
     */
    private function  getClassName($site, $sport)
    {
        // ugly, I know
        $class = '\\MyApp\Scrapers\\' . $site . '\\' . ucfirst($sport) . 'Scraper';
        return $class;
    }

}