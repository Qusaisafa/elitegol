<?php

namespace MyApp\Helpers;

class DateHelper
{
    public static function get_utc_from_date($datetime, $timezone = 'Europe/Madrid')
    {
        return self::_date_convert($datetime, $timezone, 'CET');
    }

    public static function get_date_from_utc($datetime, $timezone = 'Europe/Madrid')
    {
        return self::_date_convert($datetime, 'CET', $timezone);
    }

    /**
     * Date Conversion to TimeZone
     *
     * @param $dt  The DateTime
     * @param $tz1 Set original Timezone
     * @param $tz2 The Timezone to convert
     * @param $format string The Format
     * @return string
     */
    private static function _date_convert($dt, $tz1, $tz2, $format = "Y-m-d H:i:s")
    {
        // create DateTime object
        $d = \DateTime::createFromFormat($format, $dt, new \DateTimeZone($tz1));
        // convert timezone
        $d->setTimeZone(new \DateTimeZone($tz2));
        // convert dateformat
        return $d->format($format);
    }

}