<?php
namespace MyApp\Controllers;

use MyApp\Models\Event;
use SlimFacades\App;

class HomeController extends BaseController
{

    public function index()
    {
        $events = Event::getFrontPageEvents();

        $this->data['title'] = 'Ver eventos online / TELEFIVEGB.COM';
        $this->data['events'] = $events;

        App::render('index.twig', $this->data);
    }
}