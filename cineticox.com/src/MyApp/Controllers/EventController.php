<?php
namespace MyApp\Controllers;

use MyApp\Helpers\EventHelper;
use MyApp\Models\Channel;
use MyApp\Models\Event;
use SlimFacades\App;
use \Response;

class EventController extends BaseController
{
    public function show($id, $slug = null)
    {
        $event = Event::getByIdAndSlug($id, $slug);

        if (!$event) {
            Response::redirect($this->siteUrl('/'));
        }

        $channels = null;
        $is_live = EventHelper::isLive($event->published_at, $event->finished_at);

        if ($is_live) {
            $channels = Channel::getChannelsByEventId($event->id);
        } else {
            $in_time = EventHelper::before10minutes($event->published_at, $event->finished_at);
            $in_time = isset($in_time) ? $in_time : null;

            if ($in_time) {
                $channels = Channel::getChannelsByEventId($event->id);
            }
        }

        $this->data['title'] = $event->title;
        $this->data['event'] = $event;
        $this->data['channels'] = $channels;

        App::render('event/show.twig', $this->data);
    }

}