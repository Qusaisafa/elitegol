<?php
namespace MyApp\Controllers\Panel;

use MyApp\Controllers\BaseController;
use \App;
use MyApp\Models\Channel;
use MyApp\Models\Event;
use MyApp\Models\Link;
use SlimFacades\Request;
use \View;
use \Input;
use \Sentry;
use \Response;

class PanelController extends BaseController
{
    public function index()
    {
        View::display('panel/index.twig', $this->data);
    }

    public function guide()
    {
        $user_id = Sentry::getUser()->id;
        $channels = Channel::getChannelsByUser($user_id);
        $events = Event::getEventsWithChannels($user_id);

        $this->loadJs('common.js');
        $this->loadJs('panel/panel.js');

        $this->data['channels'] = $channels;
        $this->data['events'] = $events;
        $this->data['languages'] = require APP_PATH . '/config/languages.php';

        View::display('panel/guide.twig', $this->data);
    }

    public function channel($id)
    {
        $user_id = Sentry::getUser()->id;
        $channel = Channel::getOwnChannelById($user_id, $id);

        if (!$channel) {
            Response::redirect($this->siteUrl('/'));
        }

        $events = Event::getEventsWithoutChannels($user_id);

        $this->data['channel'] = $channel;
        $this->data['events'] = $events;
        $this->data['languages'] = require APP_PATH . '/config/languages.php';

        View::display('panel/channel.twig', $this->data);
    }

    public function save_link($channel_id)
    {

        $collection_selection = Input::post('collection_selection');

        if (empty($collection_selection)) {
            App::flash('message', 'Ningún evento seleccionado.');
            return Response::redirect(App::urlFor('panel_channel', array('id' => $channel_id)));
        }

        $channel = Channel::where('id', '=', $channel_id)->first();
        $default_language = $channel->language;

        foreach ($collection_selection as $event_id) {

            $link = $this->get_link($event_id, $channel_id);

            if (!$link) {
                $link = new Link();
                $link->event_id = $event_id;
                $link->channel_id = $channel_id;
                $link->status = true;
                $link->language = $default_language;
                $link->save();
            }

        }

        App::flash('success', 'El Canal ha sido añadido a los eventos');
        Response::redirect(App::urlFor('panel_guide'));

    }

    public function remove_link($id)
    {
        Link::destroy($id);
        App::flash('success', 'El enlace del canal ha sido removido del Evento');
        Response::redirect(App::urlFor('panel_guide'));

    }

    public function update_channel($id)
    {
        if (Request::isAjax()) {

            try {

                $link_id = Input::post('link_id');
                $language = Input::post('language');

                //$user_id = Sentry::getUser()->id;
                //$channel = Channel::getOwnChannelById($user_id, $id);

                $link = Link::where('id', '=', $link_id)->first();
                $link->language = $language;
                $link->save();
            } catch (\Exception $e) {
                echo $e;
            }
        }
    }

    private function get_link($event, $channel)
    {
        return Link::where('event_id', '=', $event)
            ->where('channel_id', '=', $channel)->first();
    }

}