<?php
namespace MyApp\Controllers\Admin;

use \App;
use MyApp\Models\Competition;
use MyApp\Models\Sport;
use \View;
use \Input;
use \Sentry;
use \Response;

class CompetitionController extends AdminBaseController
{
    public function index($id)
    {
        $sport = Sport::where('id', '=', $id)->first();
        $competitions = Competition::getCompetitionsBySportId($sport->id);

        $this->data['title'] = 'Competiciones de ' . $sport->name;
        $this->data['competitions'] = $competitions;
        View::display('admin/competitions/index.twig', $this->data);
    }

    public function create()
    {
        $this->loadSelect2();
        $competition = new Competition();
        $sports = Sport::all();

        $this->data['title'] = 'Nueva Competición';
        $this->data['competition'] = $competition;
        $this->data['sports'] = $sports;
        View::display('admin/competitions/form.twig', $this->data);
    }

    public function edit($id)
    {
        $this->loadSelect2();
        $competition = $this->getModel($id);
        $sports = Sport::all();

        $this->data['title'] = 'Editar Competición: ' . $competition->name;
        $this->data['competition'] = $competition;
        $this->data['sports'] = $sports;
        View::display('admin/competitions/form.twig', $this->data);
    }

    public function save($id = null)
    {
        $input = Input::post('competition');

        $obj = $this->getModel($id);
        $obj->name = $input['name'];
        $obj->slug = $input['slug'];
        $obj->description = $input['description'];
        $obj->sport_id = $input['sport_id'];
        $obj->alias = $input['alias'];
        $obj->status = $input['status'];

        if ($obj->isValidate()) {
            $obj->save();
            App::flash('success', sprintf("La competición %s ha sido actualizado.", $obj->name));
        }
        Response::redirect(App::urlFor('admin_competitions', array(
            'sport_id' => $input['sport_id']
        )));
    }

    private function getModel($id = null)
    {
        if (is_null($id)) {
            $model = new Competition();
        } else {
            $model = Competition::where('id', '=', $id)->first();
        }
        return $model;
    }
}
