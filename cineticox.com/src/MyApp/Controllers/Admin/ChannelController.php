<?php
namespace MyApp\Controllers\Admin;

use \App;
use Illuminate\Database\QueryException;
use MyApp\Models\Channel;
use MyApp\Models\Link;
use MyApp\Models\User;
use \View;
use \Input;
use \Sentry;
use \Response;

class ChannelController extends AdminBaseController
{
    public function index()
    {
        $this->loadJs('common.js');
        $channels = Channel::all();
        $this->data['title'] = 'Canales';
        $this->data['channels'] = $channels;
        View::display('admin/channels/index.twig', $this->data);
    }

    public function create()
    {
        $this->loadSelect2();

        $channel = new Channel();
        $this->data['title'] = 'Nuevo Canal';
        $this->data['channel'] = $channel;
        $this->data['users'] = User::all();
        $this->data['languages'] = require APP_PATH . '/config/languages.php';
        $this->data['softwares'] = $this->getSoftware();
        View::display('admin/channels/form.twig', $this->data);
    }

    public function edit($id)
    {
        $this->loadSelect2();

        $channel = $this->getModel($id);

        $this->data['title'] = 'Editar Canal: ' . $channel->title;
        $this->data['channel'] = $channel;
        $this->data['users'] = User::all();
        $this->data['languages'] = require APP_PATH . '/config/languages.php';
        $this->data['softwares'] = $this->getSoftware();
        View::display('admin/channels/form.twig', $this->data);
    }

    public function save($id = null)
    {
        $input = Input::post('channel');
        $obj = $this->getModel($id);
        $obj->user_id = $input['user_id'];
        $obj->title = $input['title'];
        $obj->url = $input['url'];
        $obj->language = $input['language'];
        $obj->quality = $input['quality'];
        $obj->software = $input['software'];
        $obj->status = $input['status'];

        try {

            if ($obj->isValidate()) {
                $obj->save();
                App::flash('success', sprintf("El Canal %s ha sido actualizado.", $obj->name));
            }
        } catch (QueryException $e) {
            App::flash('message', $e->getMessage());
        }
        Response::redirect(App::urlFor('admin_channels'));
    }

    public function delete($id)
    {
        $affectedRows = Link::where('channel_id', '=', $id)->delete();
        Channel::destroy($id);
        App::flash('success', sprintf("El Canal ha sido eliminado"));
        Response::redirect(App::urlFor('admin_channels'));
    }

    private function getModel($id = null)
    {
        if (is_null($id)) {
            $model = new Channel();
        } else {
            $model = Channel::where('id', '=', $id)->first();
        }
        return $model;
    }

    private function getSoftware()
    {
        return array(
            'flash',
            'html',
            'm3u',
            'sopcast'
            );
    }
}