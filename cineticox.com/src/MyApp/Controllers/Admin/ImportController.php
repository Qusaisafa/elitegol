<?php
namespace MyApp\Controllers\Admin;

use MyApp\Models\Event;
use MyApp\Scrapers\ScraperFactory;
use SlimFacades\App;
use SlimFacades\Input;
use SlimFacades\Response;
use \View;
use \Sentry;

class ImportController extends AdminBaseController
{
    public function la_cronica_virtual()
    {
        $this->data['title'] = 'La Cronica Virtual';
        View::display('admin/import/la_cronica_virtual/index.twig', $this->data);
    }

    public function la_cronica_virtual_sport($sport)
    {
        $this->loadSelect2();
        $this->loadDataTable();
        $this->loadJs('admin/admin.js');

        $import = new ScraperFactory('LaCronicaVirtual', strtolower($sport));
        $events = $import->getAll();

        $_SESSION['import_events'] = $events;
        $this->data['title'] = 'La Cronica Virtual - ' . ucfirst($sport);
        $this->data['subtitle'] = ucfirst($sport);
        $this->data['import_events'] = $events;
        View::display('admin/import/la_cronica_virtual/import.twig', $this->data);
    }

    public function batch_action_la_cronica($sport)
    {
        $events = $_SESSION['import_events'];
        $collection_selection = Input::post('collection_selection');

        if (empty($collection_selection)) {
            App::flash('message', 'Ningún evento seleccionado.');
            return Response::redirect(App::urlFor('admin_events'));
        }

        foreach ($collection_selection as $id) {
            $import_event = $events[$id];

            $event = new Event();
            $event->title = $import_event->title;
            $event->slug = $import_event->slug;
            $event->competition_id = $import_event->competition_id;
            $event->description = "";
            $event->published_at = $import_event->published_at;
            $event->finished_at = $import_event->finished_at;
            $event->status = 1;
            $event->save();
        }

        $_SESSION['import_events'] = null;

        App::flash('success', 'Eventos creados');
        Response::redirect(App::urlFor('admin_events'));
    }
}