<?php
namespace MyApp\Controllers\Admin;

use \App;
use MyApp\Models\Competition;
use MyApp\Models\Event;
use MyApp\Models\Sport;
use \View;
use \Input;
use \Sentry;
use \Response;

class SportController extends AdminBaseController
{
    public function index()
    {
        $sports = Sport::all();
        $this->data['title'] = 'Deportes';
        $this->data['sports'] = $sports;
        View::display('admin/sports/index.twig', $this->data);
    }

    public function create()
    {
        $sport = new Sport();
        $this->data['title'] = 'Nuevo Deporte';
        $this->data['sport'] = $sport;
        View::display('admin/sports/form.twig', $this->data);
    }

    public function edit($id)
    {
        $sport = $this->getModel($id);
        $this->data['title'] = 'Editar Deporte: ' . $sport->name;
        $this->data['sport'] = $sport;
        View::display('admin/sports/form.twig', $this->data);
    }

    public function save($id = null)
    {
        $input = Input::post('sport');
        $obj = $this->getModel($id);
        $obj->name = $input['name'];
        $obj->slug = $input['slug'];

        if ($obj->isValidate()) {
            $obj->save();
            App::flash('success', sprintf("El Deporte %s ha sido actualizado.", $obj->name));
        }
        Response::redirect(App::urlFor('admin_sports'));
    }

    private function getModel($id = null)
    {
        if (is_null($id)) {
            $model = new Sport();
        } else {
            $model = Sport::where('id', '=', $id)->first();
        }
        return $model;
    }
}