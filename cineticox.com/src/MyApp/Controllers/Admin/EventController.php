<?php
namespace MyApp\Controllers\Admin;

use \App;
use MyApp\Helpers\DateHelper;
use MyApp\Models\Competition;
use MyApp\Models\Event;
use MyApp\Models\Link;
use \View;
use \Input;
use \Sentry;
use \Response;

class EventController extends AdminBaseController
{
    public function index()
    {
        $this->loadDataTable();
        $this->loadJs('common.js');
        $events = Event::getActiveEvents();
        $this->data['events'] = $events;
        View::display('admin/events/index.twig', $this->data);
    }

    public function create()
    {
        $this->loadSelect2();
        $this->loadDateTimePicker();

        $event = new Event();
        $competitions = Competition::all();

        $this->data['event'] = $event;
        $this->data['competitions'] = $competitions;
        View::display('admin/events/form.twig', $this->data);
    }

    public function edit($id)
    {
        $this->loadSelect2();
        $this->loadDateTimePicker();

        $event = $this->getModel($id);
        $competitions = Competition::all();

        $this->data['event'] = $event;
        $this->data['competitions'] = $competitions;
        View::display('admin/events/form.twig', $this->data);
    }

    public function save($id = null)
    {
        $input = Input::post('event');

        $published_at = DateHelper::get_utc_from_date($input['published_at']);
        $finished_at = DateHelper::get_utc_from_date($input['finished_at']);

        $obj = $this->getModel($id);
        $obj->title = $input['title'];
        $obj->slug = $input['slug'];
        $obj->description = $input['description'];
        $obj->competition_id = $input['competition_id'];
        $obj->published_at = $published_at;
        $obj->finished_at = $finished_at;
        $obj->status = $input['status'];

        if ($obj->isValidate()) {
            $obj->save();
            App::flash('success', sprintf("El Evento %s ha sido creado.", $obj->title));
        }
        Response::redirect(App::urlFor('admin_events'));
    }

    public function delete($id)
    {
        $affectedRows = Link::where('event_id', '=', $id)->delete();
        Event::destroy($id);
        App::flash('success', sprintf("Evento eliminado"));
        Response::redirect(App::urlFor('admin_events'));
    }

    private function getModel($id = null)
    {
        if (is_null($id)) {
            $model = new Event();
        } else {
            $model = Event::where('id', '=', $id)->first();
        }
        return $model;
    }
}