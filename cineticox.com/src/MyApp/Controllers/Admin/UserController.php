<?php
namespace MyApp\Controllers\Admin;

use \App;
use MyApp\Models\Competition;
use MyApp\Models\Event;
use MyApp\Models\User;
use \View;
use \Input;
use \Sentry;
use \Response;

class UserController extends AdminBaseController
{
    public function index()
    {
        $users = User::all();
        $this->data['title'] = 'Usuarios';
        $this->data['users'] = $users;
        View::display('admin/users/index.twig', $this->data);
    }

    public function create()
    {
        $event = new Event();
        $competitions = Competition::all();

        $this->data['event'] = $event;
        $this->data['competitions'] = $competitions;
        View::display('admin/events/form.twig', $this->data);
    }

    public function edit($id)
    {
        $event = $this->getModel($id);
        $competitions = Competition::all();

        $this->data['event'] = $event;
        $this->data['competitions'] = $competitions;
        View::display('admin/events/form.twig', $this->data);
    }

    public function save($id = null)
    {
        $input = Input::post('event');
        $obj = $this->getModel($id);
        $obj->title = $input['title'];
        $obj->slug = $input['slug'];
        $obj->description = $input['description'];
        $obj->competition_id = $input['competition_id'];
        $obj->published_at = $input['published_at'];
        $obj->finished_at = $input['finished_at'];
        $obj->status = $input['status'];

        if ($obj->isValidate()) {
            $obj->save();
            App::flash('success', sprintf("El Evento %s ha sido creado.", $obj->title));
        }
        Response::redirect(App::urlFor('admin_events'));
    }

    private function getModel($id = null)
    {
        if (is_null($id)) {
            $model = new Event();
        } else {
            $model = Event::where('id', '=', $id)->first();
        }
        return $model;
    }
}