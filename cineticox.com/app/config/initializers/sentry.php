<?php
//use Cartalyst\Sentry\Facades\Native\Sentry;

class_alias('Cartalyst\Sentry\Facades\Native\Sentry', 'Sentry');

$app->container->singleton(
    'auth',
    function () {
        $hasher = new Cartalyst\Sentry\Hashing\NativeHasher;
        $userProvider = new Cartalyst\Sentry\Users\Eloquent\Provider($hasher);
        $groupProvider = new Cartalyst\Sentry\Groups\Eloquent\Provider;
        $throttleProvider = new Cartalyst\Sentry\Throttling\Eloquent\Provider($userProvider);
        $session = new Cartalyst\Sentry\Sessions\NativeSession;
        $cookie = new Cartalyst\Sentry\Cookies\NativeCookie;

        /* @var Cartalyst\Sentry\Facades\Native\Sentry $sentry */
        $sentry = new Sentry(
            $userProvider,
            $groupProvider,
            $throttleProvider,
            $session,
            $cookie
        );

        return $sentry::instance();
    }
);