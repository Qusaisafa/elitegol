<?php

$app->view(new \Slim\Views\Twig());

$app->view->parserOptions = array(
    'debug' => true,
    'charset' => 'utf-8',
    'cache' => realpath(CACHE_PATH),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => false,
    'twigTemplateDirs' => array('../app/views/'),
    'twigExtensions' => array(
        'Twig_Extensions_Slim'
    )
);

$app->view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
    new Twig_Extensions_Extension_Intl(),
    new \MyApp\Twig\DateTimeExtension(),
    new \MyApp\Twig\EventExtension(),
    new \MyApp\Twig\UserExtension(),
    new \Twig_Extension_Debug
);

//$twig = $app->view->getInstance();
//$twig->addGlobal('Sentry', new \Sentry);
