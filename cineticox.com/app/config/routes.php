<?php

Route::group(
    '/admin',
    function () {
        if (!Sentry::check()) {
            if (Request::isAjax()) {
                Response::headers()->set('Content-Type', 'application/json');
                Response::setBody(json_encode(
                    array(
                        'success' => false,
                        'message' => 'Session expired or unauthorized access.',
                        'code' => 401
                    )
                ));
                App::stop();
            } else {
                $redirect = Request::getResourceUri();
                Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
            }
        }
    },
    function () use ($app) {
        Route::get('/', 'MyApp\Controllers\Admin\AdminController:index')->name('admin');

        // Events
        Route::get('/events', 'MyApp\Controllers\Admin\EventController:index')->name('admin_events');
        Route::get('/event', 'MyApp\Controllers\Admin\EventController:create')->name('admin_event');
        Route::post('/event', 'MyApp\Controllers\Admin\EventController:save');
        Route::get('/event(/:id)', 'MyApp\Controllers\Admin\EventController:edit')->name('admin_event_edit');
        Route::post('/event(/:id)', 'MyApp\Controllers\Admin\EventController:save');
        Route::get('/event(/:id)/delete', 'MyApp\Controllers\Admin\EventController:delete')->name('admin_event_delete');

        // Sports
        Route::get('/sports', 'MyApp\Controllers\Admin\SportController:index')->name('admin_sports');
        Route::get('/sport', 'MyApp\Controllers\Admin\SportController:create')->name('admin_sport');
        Route::post('/sport', 'MyApp\Controllers\Admin\SportController:save');
        Route::get('/sport(/:id)', 'MyApp\Controllers\Admin\SportController:edit')->name('admin_sport_edit');
        Route::post('/sport(/:id)', 'MyApp\Controllers\Admin\SportController:save');

        // Competitions
        Route::get('/sport(/:sport_id)/competitions', 'MyApp\Controllers\Admin\CompetitionController:index')
            ->name('admin_competitions');

        Route::get('/competition', 'MyApp\Controllers\Admin\CompetitionController:create')->name('admin_competition');
        Route::post('/competition', 'MyApp\Controllers\Admin\CompetitionController:save');

        Route::get('/competition(/:id)', 'MyApp\Controllers\Admin\CompetitionController:show')
            ->name('admin_competition_show');

        Route::get('/competition(/:id)/edit', 'MyApp\Controllers\Admin\CompetitionController:edit')
            ->name('admin_competition_edit');
        Route::post('/competition(/:id)/edit', 'MyApp\Controllers\Admin\CompetitionController:save');

        // Channels
        Route::get('/channels', 'MyApp\Controllers\Admin\ChannelController:index')->name('admin_channels');
        Route::get('/channel', 'MyApp\Controllers\Admin\ChannelController:create')->name('admin_channel');
        Route::post('/channel', 'MyApp\Controllers\Admin\ChannelController:save');
        Route::get('/channel(/:id)/edit', 'MyApp\Controllers\Admin\ChannelController:edit')->name('admin_channel_edit');
        Route::post('/channel(/:id)/edit', 'MyApp\Controllers\Admin\ChannelController:save');
        Route::get('/channel(/:id)/delete', 'MyApp\Controllers\Admin\ChannelController:delete')->name('admin_channel_delete');

        // Users
        Route::get('/users', 'MyApp\Controllers\Admin\UserController:index')->name('admin_users');

        // Scrapers Tools
        Route::get('/import/la_cronica_virtual',
            'MyApp\Controllers\Admin\ImportController:la_cronica_virtual')->name('import_la_cronica_virtual');
        Route::get('/import/la_cronica_virtual(/:sport)',
            'MyApp\Controllers\Admin\ImportController:la_cronica_virtual_sport')->name('import_la_cronica_virtual_sport');
        Route::post('/import/la_cronica_virtual(/:sport)',
            'MyApp\Controllers\Admin\ImportController:batch_action_la_cronica');
    }
);

Route::group(
    '/panel',
    function () {
        if (!Sentry::check()) {
            if (Request::isAjax()) {
                Response::headers()->set('Content-Type', 'application/json');
                Response::setBody(json_encode(
                    array(
                        'success' => false,
                        'message' => 'Session expired or unauthorized access.',
                        'code' => 401
                    )
                ));
                App::stop();
            } else {
                $redirect = Request::getResourceUri();
                Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
            }
        }
    },
    function () use ($app) {
        Route::get('/', 'MyApp\Controllers\Panel\PanelController:index')->name('panel');
        Route::get('/indexado', 'MyApp\Controllers\Panel\PanelController:guide')->name('panel_guide');
        Route::get('/programar-canal(/:id)', 'MyApp\Controllers\Panel\PanelController:channel')->name('panel_channel');
        Route::post('/programar-canal(/:id)', 'MyApp\Controllers\Panel\PanelController:save_link');
        //Route::post('/link', 'MyApp\Controllers\Panel\PanelController:link')->name('panel_link');
        Route::get('/remove_link(/:id)', 'MyApp\Controllers\Panel\PanelController:remove_link')->name('panel_remove_link');
        Route::post('/channel(/:id)/', 'MyApp\Controllers\Panel\PanelController:update_channel')->name('panel_channel_update');
    }
);

// Auth
Route::get('/login', 'MyApp\Controllers\AuthController:login')->name('login');
Route::get('/logout', 'MyApp\Controllers\AuthController:logout')->name('logout');
Route::post('/login', 'MyApp\Controllers\AuthController:doLogin');
Route::get('/registro', 'MyApp\Controllers\AuthController:register')->name('register');
Route::post('/registro', 'MyApp\Controllers\AuthController:doRegister');

// Front-End
Route::get('/', 'MyApp\Controllers\HomeController:index');
Route::get('/evento(/:id)(/:slug)', 'MyApp\Controllers\EventController:show')->name('event');
Route::get('/ajax/abrir_evento(/:id)', 'MyApp\Controllers\AjaxController:open_event');

// CRON & Tasks
Route::get('/tasks/scheduled', 'MyApp\Controllers\Task\ScheduledController:task_scheduled')->name('task_scheduled');
Route::get('/cron/tasks/scheduled', 'MyApp\Controllers\Task\ScheduledController:scheduled');