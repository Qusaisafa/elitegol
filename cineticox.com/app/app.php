<?php

session_cache_limiter(false);
session_start();

setlocale(LC_ALL, "es_ES");
date_default_timezone_set('Europe/Lisbon');

//define( 'ROOT_PATH', __DIR__ );
define( 'ROOT_PATH', dirname( dirname(__FILE__) ) );
define( 'APP_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'app' );
define( 'CACHE_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'cache' );

require 'vendor/autoload.php';

use Slim\Slim as Application;
use SlimFacades\Facade;

$app = new Application(array(
    'debug' => true,
    'mode' => "development", //production
    'templates.path' => APP_PATH .'/views',
));

// Initialize the Facade class
Facade::setFacadeApplication($app);
Facade::registerAliases();

//foreach (glob(APP_PATH . ' /config/*.php') as $configFile) {
//    require $configFile;
//}

// Initializers
require APP_PATH . '/config/initializers/eloquent.php';
require APP_PATH . '/config/initializers/sentry.php';
require APP_PATH . '/config/initializers/twig.php';
require APP_PATH . '/config/initializers/middleware.php';

// App
require APP_PATH . '/config/routes.php';
require APP_PATH . '/config/settings.php';

