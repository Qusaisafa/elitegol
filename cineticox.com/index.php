<?php

if ( empty( $app ) ) {
	require __DIR__ . '/app/app.php';
}

$app->run();