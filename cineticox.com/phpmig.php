<?php

use \Phpmig\Pimple\Pimple,
	\Illuminate\Database\Capsule\Manager as Capsule,
	\Phpmig\Adapter;

$container = new Pimple();

$container['schema'] = $container->share( function ( $c ) {
	$database = require __DIR__ . '/app/config/database.php';
	/* Bootstrap Eloquent */
	$capsule = new Capsule;
	$capsule->addConnection( $database );
	$capsule->setAsGlobal();

	return Capsule::schema();
} );

$container['db'] = $container->share( function () {
	$database   = require __DIR__ . '/app/config/database.php';
	$connection = sprintf( "mysql:dbname=%s;host=localhost", $database['database'] );
	$dbh        = new PDO( $connection, $database['username'], $database['password'] );
	$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

	return $dbh;
} );

$container['phpmig.adapter'] = $container->share( function () use ( $container ) {
	return new Adapter\PDO\Sql( $container['db'], 'migrations' );
} );

$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'db' . DIRECTORY_SEPARATOR . 'migrations';

return $container;
