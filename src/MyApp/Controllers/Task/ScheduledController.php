<?php
namespace MyApp\Controllers\Task;

use Carbon\Carbon;
use Slim\Slim;
use MyApp\Models\Event;

class ScheduledController
{
    protected $app;

    public function __construct()
    {
        $this->app = Slim::getInstance();
    }

    public function task_scheduled()
    {
        $tz = $this->app->config('tz');
        $current_time = Carbon::now($tz);

        $events = Event::where('status', '=', '1')
            ->where('published_at', '<', $current_time)
            ->where('finished_at', '<', $current_time)
            ->get();

        foreach ($events as $event) {
            $event->status = 0;
            $event->save();
        }

        echo "Ola ke ace";
    }
}