<?php
namespace MyApp\Controllers;

use \App;
use Cartalyst\Sentry\Users\UserExistsException;
use SlimFacades\Route;
use \View;
use \Input;
use \Sentry;
use \Response;

class AuthController extends BaseController
{
    /**
     * display the login form
     */
    public function login()
    {
        if (Sentry::check()) {
            Response::redirect($this->siteUrl('/'));
        } else {
            $this->data['redirect'] = (Input::get('redirect')) ? base64_decode(Input::get('redirect')) : '';
            View::display('auth/login.twig', $this->data);
        }
    }

    /**
     * Process the login
     */
    public function doLogin()
    {
        $remember = Input::post('remember', false);
        $email = Input::post('email');
        $redirect = Input::post('redirect');
        $redirect = ($redirect) ? $redirect : '/';
        try {
            $credential = array(
                'email' => $email,
                'password' => Input::post('password')
            );
            // Try to authenticate the user
            $user = Sentry::authenticate($credential, false);
            if ($remember) {
                Sentry::loginAndRemember($user);
            } else {
                Sentry::login($user, false);
            }
            Response::redirect($this->siteUrl($redirect));
        } catch (\Exception $e) {
            App::flash('message', $e->getMessage());
            App::flash('email', $email);
            App::flash('redirect', $redirect);
            App::flash('remember', $remember);
            Response::redirect($this->siteUrl('login'));
        }
    }

    /**
     * Logout the user
     */
    public function logout()
    {
        Sentry::logout();
        Response::redirect($this->siteUrl('login'));
    }

    /**
     * display the register form
     */
    public function register()
    {
        if (Sentry::check()) {
            Response::redirect($this->siteUrl('/'));
        } else {
            $this->data['redirect'] = (Input::get('redirect')) ? base64_decode(Input::get('redirect')) : '';
            View::display('auth/register.twig', $this->data);
        }
    }

    /**
     * Process the register
     */
    public function doRegister()
    {
        $user = null;
        $message = '';
        $success = false;

        try {
            $input = Input::post();
            /*if ($input['password'] != $input['confirm_password']) {
                App::flash('message', 'La contraseña y la confirmación de contraseña no coinciden');
                Response::redirect($this->siteUrl('registro'));
                throw new UserExistsException("A user already exists with login [$login], logins must be unique for users.");
            }*/

            $user = Sentry::createUser(array(
                'email' => $input['email'],
                'password' => $input['password'],
                'activated' => 1
            ));

            // Add to Group
            $userGroup = Sentry::findGroupById(2);
            $user->addGroup($userGroup);

            $credential = array(
                'email' => $input['email'],
                'password' => $input['password']
            );

            // Try to authenticate the user
            //$user = Sentry::authenticate($credential, false);
            Sentry::login($user, false);
            Response::redirect($this->siteUrl('/'));

        } catch (UserExistsException $e) {
            App::flash('message', "El e-mail está en uso.");
        } catch (\Exception $e) {
            App::flash('message', $e->getMessage());
        }
        Response::redirect($this->siteUrl('registro'));
    }
}