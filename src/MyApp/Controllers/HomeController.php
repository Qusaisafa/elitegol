<?php
namespace MyApp\Controllers;

use MyApp\Models\Event;
use SlimFacades\App;

class HomeController extends BaseController
{

    public function index()
    {
        $events = Event::getFrontPageEvents();

        $this->data['title'] = 'ELITEGOL.ONLINE | PIRLO TV | Rojadirecta  - Tarjeta Roja TV - Fútbol En Vivo - EliteGOL';
        $this->data['events'] = $events;

        App::render('index.twig', $this->data);
    }
}