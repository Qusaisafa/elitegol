<?php
namespace MyApp\Controllers\Panel;

use MyApp\Controllers\BaseController;
use MyApp\Helpers\DateHelper;

use \App;
use MyApp\Models\Channel;
use MyApp\Models\Competition;
use MyApp\Models\Event;
use MyApp\Models\Link;
use SlimFacades\Request;
use \View;
use \Input;
use \Sentry;
use \Response;

class PanelController extends BaseController
{
    public function index()
    {
        View::display('panel/index.twig', $this->data);
    }

    public function guide()
    {
        $user_id = Sentry::getUser()->id;
        $channels = Channel::getChannelsByUser($user_id);
        $events = Event::getEventsWithChannels($user_id);

        $this->loadJs('common.js');
        $this->loadJs('panel/panel.js');

        $this->data['channels'] = $channels;
        $this->data['events'] = $events;
        $this->data['languages'] = require APP_PATH . '/config/languages.php';

        View::display('panel/guide.twig', $this->data);
    }

    public function channel($id)
    {
        $user_id = Sentry::getUser()->id;
        $channel = Channel::getOwnChannelById($user_id, $id);

        if (!$channel) {
            Response::redirect($this->siteUrl('/'));
        }

        $events = Event::getEventsWithoutChannels($user_id);

        $this->data['channel'] = $channel;
        $this->data['events'] = $events;
        $this->data['languages'] = require APP_PATH . '/config/languages.php';

        View::display('panel/channel.twig', $this->data);
    }

    public function save_link($channel_id)
    {

        $collection_selection = Input::post('collection_selection');

        if (empty($collection_selection)) {
            App::flash('message', 'Ningún evento seleccionado.');
            return Response::redirect(App::urlFor('panel_channel', array('id' => $channel_id)));
        }

        $channel = Channel::where('id', '=', $channel_id)->first();
        $default_language = $channel->language;

        foreach ($collection_selection as $event_id) {

            $link = $this->get_link($event_id, $channel_id);

            if (!$link) {
                $link = new Link();
                $link->event_id = $event_id;
                $link->channel_id = $channel_id;
                $link->status = true;
                $link->language = $default_language;
                $link->save();
            }

        }

        App::flash('success', 'El Canal ha sido añadido a los eventos');
        Response::redirect(App::urlFor('panel_guide'));

    }

    public function remove_link($id)
    {
        Link::destroy($id);
        App::flash('success', 'El enlace del canal ha sido removido del Evento');
        Response::redirect(App::urlFor('panel_guide'));

    }

    public function update_channel($id)
    {
        if (Request::isAjax()) {

            try {

                $link_id = Input::post('link_id');
                $language = Input::post('language');

                //$user_id = Sentry::getUser()->id;
                //$channel = Channel::getOwnChannelById($user_id, $id);

                $link = Link::where('id', '=', $link_id)->first();
                $link->language = $language;
                $link->save();
            } catch (\Exception $e) {
                echo $e;
            }
        }
    }
    public function la_qu_channel_update($id)
    {
        $this->loadSelect2();

        $channel = $this->getModelChannel($id);

        $this->data['title'] = 'Editar Canal: ' . $channel->title;
        $this->data['channel'] = $channel;
        $this->data['languages'] = require APP_PATH . '/config/languages.php';
//        $this->data['softwares'] = $this->getSoftware();
        View::display('panel/update_channel.twig', $this->data);
    }

    private function get_link($event, $channel)
    {
        return Link::where('event_id', '=', $event)
            ->where('channel_id', '=', $channel)->first();
    }
    public function create_user_event()
    {
        $this->loadSelect2();
        $this->loadDateTimePicker();

        $event = new Event();
        $competitions = Competition::all();

        $this->data['event'] = $event;
        $this->data['competitions'] = $competitions;
        View::display('panel/user_event.twig', $this->data);
    }
    public function save_channel($id = null)
    {
        $input = Input::post('channel');
        $obj = $this->getModelChannel($id);
//        $obj->user_id = $input['user_id'];
//        $obj->title = $input['title'];
//        $obj->url = $input['url'];
        $obj->language = $input['language'];
        $obj->quality = $input['quality'];
//        $obj->software = $input['software'];
//        $obj->status = $input['status'];

        try {

            if ($obj->isValidate()) {
                $obj->save();
                App::flash('success', sprintf("El Canal %s ha sido actualizado.", $obj->name));
            }
        } catch (QueryException $e) {
            App::flash('message', $e->getMessage());
        }
        Response::redirect(App::urlFor('panel_guide'));
    }
    public function save_user_event($id = null)
    {
        $input = Input::post('event');

        //$published_at = DateHelper::get_utc_from_date($input['published_at']);
        //$finished_at = DateHelper::get_utc_from_date($input['finished_at']);
        //get date without convert to other time zone just take the time as is
        $published_at = DateHelper::get_date_without_convert($input['published_at']);
        $finished_at = DateHelper::get_date_without_convert($input['finished_at']);

        $obj = $this->getModel($id);
        $obj->title = $input['title'];
        $obj->slug = $input['slug'];
        $obj->description = $input['description'];
        $obj->competition_id = $input['competition_id'];
        $obj->published_at = $published_at;
        $obj->finished_at = $finished_at;
        $obj->status = $input['status'];

        if ($obj->isValidate()) {
            $obj->save();
            App::flash('success', sprintf("El Evento %s ha sido creado.", $obj->title));
        }
        Response::redirect(App::urlFor('create_user_event'));
    }

    private function getModel($id = null)
    {
        if (is_null($id)) {
            $model = new Event();
        } else {
            $model = Event::where('id', '=', $id)->first();
        }
        return $model;
    }
    private function getModelChannel($id = null)
    {
        if (is_null($id)) {
            $model = new Channel();
        } else {
            $model = Channel::where('id', '=', $id)->first();
        }
        return $model;
    }
    public function loadDateTimePicker()
    {
        $external = array('location' => 'external');
        $this->loadCss("https://cdn.rawgit.com/smalot/bootstrap-datetimepicker/master/css/bootstrap-datetimepicker.min.css", $external);

        $this->loadJs("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.1/moment.min.js", $external);
        $this->loadJs("https://cdn.rawgit.com/smalot/bootstrap-datetimepicker/master/js/bootstrap-datetimepicker.min.js", $external);
        $this->loadJs("https://cdn.rawgit.com/smalot/bootstrap-datetimepicker/master/js/locales/bootstrap-datetimepicker.es.js", $external);
        $this->loadJs('admin/datetimepicker.js');
    }
    public function loadSelect2()
    {
        $external = array('location' => 'external');
        $this->loadCss('https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css', $external);
        $this->loadCss('https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2-bootstrap.min.css', $external);

        $this->loadJs('https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js', $external);
        $this->loadJs('admin/select2.js');
    }
}