<?php
namespace MyApp\Controllers\Admin;

use MyApp\Controllers\BaseController;
use \App;
use \View;
use \Input;
use \Sentry;
use \Response;

class AdminBaseController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isAdmin();
        $this->resetCss();
        $this->resetJs();
        //$this->loadSelect2();
    }

    private function isAdmin()
    {
        $user = Sentry::getUser();

        if ($user) {
            if (!$user->hasAccess('admin')) {
                Response::redirect($this->siteUrl('/'));
            }
        }
    }

    public function loadSelect2()
    {
        $external = array('location' => 'external');
        $this->loadCss('https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css', $external);
        $this->loadCss('https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2-bootstrap.min.css', $external);

        $this->loadJs('https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js', $external);
        $this->loadJs('admin/select2.js');
    }

    public function loadDataTable()
    {
        $external = array('location' => 'external');
        //$this->loadCss("https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.4/css/jquery.dataTables.min.css", $external);
        $this->loadCss("https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css", $external);

        $this->loadJs("http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js", $external);
        $this->loadJs("https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js", $external);
        $this->loadJs('admin/datatable.js');
    }

    public function loadDateTimePicker()
    {
        $external = array('location' => 'external');
        $this->loadCss("https://cdn.rawgit.com/smalot/bootstrap-datetimepicker/master/css/bootstrap-datetimepicker.min.css", $external);

        $this->loadJs("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.1/moment.min.js", $external);
        $this->loadJs("https://cdn.rawgit.com/smalot/bootstrap-datetimepicker/master/js/bootstrap-datetimepicker.min.js", $external);
        $this->loadJs("https://cdn.rawgit.com/smalot/bootstrap-datetimepicker/master/js/locales/bootstrap-datetimepicker.es.js", $external);
        $this->loadJs('admin/datetimepicker.js');
    }
}