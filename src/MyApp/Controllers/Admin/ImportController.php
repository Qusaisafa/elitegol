<?php
namespace MyApp\Controllers\Admin;
use MyApp\Scrapers\rojadirecta\BaseScraper;
use MyApp\Models\Event;
use MyApp\Scrapers\ScraperFactory;
use SlimFacades\App;
use SlimFacades\Input;
use SlimFacades\Response;
use \View;
use \Sentry;
use Carbon\Carbon;
use MyApp\Models\Competition;


class ImportController extends AdminBaseController
{
    public $competition_data;
    public $sport_id = null;
    public $default_competition = "Amistoso";

    public function la_cronica_virtual()
    {
        $this->data['title'] = 'La Cronica Virtual';
        View::display('admin/import/la_cronica_virtual/index.twig', $this->data);
    }
    public function rojadirecta()
    {
        $this->data['title'] = 'rojadirecta';
        View::display('admin/import/rojadirecta/index.twig', $this->data);
    }

    public function la_cronica_virtual_sport($sport)
    {
        $this->loadSelect2();
        $this->loadDataTable();
        $this->loadJs('admin/admin.js');

        $import = new ScraperFactory('LaCronicaVirtual', strtolower($sport));
        $events = $import->getAll();

        $_SESSION['import_events'] = $events;
        $this->data['title'] = 'La Cronica Virtual - ' . ucfirst($sport);
        $this->data['subtitle'] = ucfirst($sport);
        $this->data['import_events'] = $events;
        View::display('admin/import/la_cronica_virtual/import.twig', $this->data);
    }

    public function rojadirecta_sport($sport)
    {
        $this->loadSelect2();
        $this->loadDataTable();
        $this->loadJs('admin/admin.js');

        $import = new ScraperFactory('rojadirecta', strtolower($sport));
        $events = $import->getAll();

        $_SESSION['import_events'] = $events;
        $this->data['title'] = 'rojadirecta - ' . ucfirst($sport);
        $this->data['subtitle'] = ucfirst($sport);
        $this->data['import_events'] = $events;
        View::display('admin/import/rojadirecta/import.twig', $this->data);
    }

    public function batch_action_la_cronica($sport)
    {
        $events = $_SESSION['import_events'];
        $collection_selection = Input::post('collection_selection');

        if (empty($collection_selection)) {
            App::flash('message', 'Ning�n evento seleccionado.');
            return Response::redirect(App::urlFor('admin_events'));
        }

        foreach ($collection_selection as $id) {
            $import_event = $events[$id];

            $event = new Event();
            $event->title = $import_event->title;
            $event->slug = $import_event->slug;
            $event->competition_id = $import_event->competition_id;
            $event->description = "";
            $event->published_at = $import_event->published_at;
            $event->finished_at = $import_event->finished_at;
            $event->status = 1;
            $event->save();
        }

        $_SESSION['import_events'] = null;

        App::flash('success', 'Eventos creados');
        Response::redirect(App::urlFor('admin_events'));
    }
    public function batch_action_rojadirecta($sport)
    {
        $events = $_SESSION['import_events'];
        $collection_selection = Input::post('collection_selection');
        $this->setCompetitionData();
        if (empty($collection_selection)) {
            App::flash('message', 'Ning�n evento seleccionado.');
            return Response::redirect(App::urlFor('admin_events'));
        }

        foreach ($collection_selection as $id) {
            $import_event = $events[$id];
            $competition = $this->_get_competition($import_event->competition_name,$import_event->type);

            $event = new Event();
            $event->title = $import_event->title;
            $event->slug = $import_event->slug;
            $event->competition_id = $competition['id'];
            $event->description = "";
            $event->published_at = $import_event->published_at;
            $event->finished_at = $import_event->finished_at;
            $event->status = 1;
            $event->save();
        }

        $_SESSION['import_events'] = null;

        App::flash('success', 'Eventos creados');
        Response::redirect(App::urlFor('admin_events'));
    }

    public function setCompetitionData()
    {
        $data = array();

//        if (!$this->sport_id){
//            throw new Exception("Error Processing Sport ID", 1);
//        }

        $collection = Competition::select('id', 'name')
//            ->where('sport_id', '=', 1 or 2 or 3)
            ->get();

        foreach ($collection as $item) {
            $data[$item->alias] = array(
                'id' => $item->id,
                'name' => $item->name
            );
        }
        $this->competition_data = $data;
    }

    public function _get_competition($name,$type)
    {
        if (isset($this->competition_data[$name])){
            $competition = $this->competition_data[$name];
        }else{ // grate new competition;
            $competition = new Competition();
            $competition->name = $name ;
            $competition->slug = $name ;
            $competition->description =" ";
            $competition->sport_id = BaseScraper::get_sport_id($type);
            $competition->status = 1;
            $competition->save();
        }

        if (!$competition) {
            return $this->competition_data[$this->default_competition];
        }

        return $competition;
    }



}