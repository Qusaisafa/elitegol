<?php
namespace MyApp\Controllers\Admin;

use \App;
use \View;
use \Input;
use \Sentry;
use \Response;

class AdminController extends AdminBaseController
{
    /**
     * display the admin dashboard
     */
    public function index()
    {
        View::display('admin/index.twig', $this->data);
    }
}