<?php
namespace MyApp\Controllers;

use MyApp\Helpers\EventHelper;
use MyApp\Models\Channel;
use MyApp\Models\Event;
use SlimFacades\App;
use \Response;
use SlimFacades\Request;

class AjaxController extends BaseController
{
    public function open_event($id)
    {
        //TODO: Mejorar esta mierda, que se hizo al paso.
        if (Request::isAjax()) {

            $event = Event::where('id', '=', $id)->first();

            $is_live = EventHelper::isLive($event->published_at, $event->finished_at);

            if ($is_live) {

                $channels = Channel::getChannelsByEventId($event->id);
                if ($channels->isEmpty()) {
                    return $this->_message('No hay enlaces disponibles para este evento deportivo.');
                }

            } else {

                $in_time = EventHelper::before10minutes($event->published_at, $event->finished_at);
                $in_time = isset($in_time) ? $in_time : null;

                if ($in_time) {
                    $channels = Channel::getChannelsByEventId($event->id);
                    if ($channels->isEmpty()) {
                        return $this->_message('No hay enlaces disponibles para este evento deportivo.');
                    }
                } else {
                    return $this->_message("Los enlaces serán mostrados <b>60 minutos</b> antes de su comienzo.</div>");
                }
            }

            $this->data['channels'] = $channels;
            App::render('event/show_ajax.twig', $this->data);

        } else {
            $this->ola_k_ase();
        }

    }

    private function _message($msg)
    {
        echo '<div class="mensaje"> ' . $msg . '</div>';
    }

    private function ola_k_ase()
    {
        $image = $this->siteUrl(' / ') . 'images / cr7 . jpg';
        echo ' < img src = "' . $image . '" alt = "OLA K ASE" />';
    }

}
