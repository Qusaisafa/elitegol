<?php

namespace MyApp\Twig;

use MyApp\Helpers\EventHelper;

class EventExtension extends \Twig_Extension
{

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('isLive', array($this, 'isLive')),
            new \Twig_SimpleFunction('before10minutes', array($this, 'before10minutes')),
            new \Twig_SimpleFunction('isToday', array($this, 'isToday')),
        );
    }

    function isLive($start_date, $end_date)
    {
        return EventHelper::isLive($start_date, $end_date);
    }

    function before10minutes($start_date, $end_date)
    {
        return EventHelper::before10minutes($start_date, $end_date);
    }

	function isToday($published_at)
	{
		return EventHelper::isToday($published_at);
	}

    public function getName()
    {
        return 'slim_event';
    }

}