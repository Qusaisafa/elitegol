<?php

namespace MyApp\Twig;

use MyApp\Helpers\DateHelper;

class DateTimeExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'get_date_from_utc' => new \Twig_SimpleFilter('get_date_from_utc', array($this, 'get_date_from_utc')),
        );
    }

    function get_date_from_utc($string, $timezone = 'Europe/Lisbon')
    {
        if ($string) {
            return DateHelper::get_date_from_utc($string, $timezone);
        }
        return '';
    }

    public function getName()
    {
        return 'slim2';
    }
}