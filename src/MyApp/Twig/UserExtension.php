<?php

namespace MyApp\Twig;

use Slim\Slim;
use \Sentry;

class UserExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'user' => new \Twig_SimpleFilter('user', array($this, 'user')),
        );
    }

    function user($string)
    {
        $app = Slim::getInstance();
        $user = Sentry::getUser();

        return $user;
    }

    public function getName()
    {
        return 'slim_user';
    }
}