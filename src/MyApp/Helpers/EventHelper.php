<?php
namespace MyApp\Helpers;

use Carbon\Carbon;

class EventHelper
{
    public static function isLive($start_date, $end_date)
    {
        $today_date = Carbon::now('Europe/Madrid');//UTC
        $today_timestamp = strtotime($today_date);

        $start_timestamp = strtotime($start_date);
        $end_timestamp = strtotime($end_date);

        return (($today_timestamp >= $start_timestamp) && ($today_timestamp <= $end_timestamp));
    }

    public static function before10minutes($start_date, $end_date)
    {
        $before40minutes = new Carbon($start_date);
        $before40minutes->subMinutes(1440);

        return static::isLive($before40minutes, $end_date);
    }

	public static function isToday($published_at)
	{
		$my_date = new Carbon($published_at);
		if ($my_date->isToday()){
			return true;
		}
		return false;
	}
}