<?php
namespace MyApp\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Link extends Model
{
    protected $fillable = array('id', 'event_id', 'channel_id', 'status');
    public $timestamps = false;
}