<?php
namespace MyApp\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Model;

class Event extends Model
{
    protected $fillable = array(
        'id',
        'title',
        'slug',
        'description',
        'competition_id',
        'published_at',
        'finished_at',
        'status'
    );

    public $timestamps = false;

    public static function isValidate()
    {
        return true;
    }

    /**
     * Get Current Events (front-end)
     *
     * @param string $tz
     * @return mixed
     */
    public static function getFrontPageEvents()
    {
        $now = Carbon::now('UTC');

        $sports = Sport::select('id')->lists('id');
        $events = Event::leftJoin('competitions', function ($join) {
            $join->on('competitions.id', '=', 'events.competition_id');
        })
            ->leftJoin('sports', function ($join) {
                $join->on('sports.id', '=', 'competitions.sport_id');
            })
            ->where('events.status', '=', 1)
            ->where('finished_at', '>=', $now)
            ->whereIn('competitions.sport_id', $sports)
            ->orderBy('events.published_at', 'ASC')
            ->get(array(
                'events.*',
                'competitions.name as competition_name',
                'sports.slug as sport',
                'sports.id as sport_id'
            ));
        //TODO: Expire cache. ->remember(5)
        return $events;
    }

    /**
     *
     * Get The Event (front-end)
     *
     * @param integer $event_id The Event ID
     * @param string $slug The Slug
     * @return mixed
     */
    public static function getByIdAndSlug($event_id, $slug)
    {
        $events = Event::leftJoin('competitions', function ($join) {
            $join->on('competitions.id', '=', 'events.competition_id');
        })
            ->where('events.id', '=', $event_id)
            ->where('events.slug', '=', $slug)
            ->first(array(
                'events.*',
                'competitions.name as competition_name',
                'competitions.slug as competition_slug'
            ));
        return $events;
    }

    /*
        SELECT events.*, competitions.name as competitions_name,
        channels.id as channel_id, channels.title as channel_title
        FROM events
        LEFT JOIN links ON links.event_id = events.id
        LEFT JOIN channels ON links.channel_id = channels.id
        LEFT JOIN competitions ON events.competition_id = competitions.id
        WHERE channels.user_id = 1
        AND events.status = 1
        GROUP BY links.id
    */

    /**
     *
     * Get Events with Channel for the User (panel)
     *
     * @param $user_id
     * @return mixed
     */
    public static function getEventsWithChannels($user_id)
    {
        $events = Event::leftJoin('links', function ($join) {
            $join->on('links.event_id', '=', 'events.id');
        })
            ->leftJoin('channels', function ($join) {
                $join->on('links.channel_id', '=', 'channels.id');
            })
            ->leftJoin('competitions', function ($join) {
                $join->on('events.competition_id', '=', 'competitions.id');
            })
            ->where('channels.user_id', '=', $user_id)
            ->where('events.status', '=', 1)
            ->groupBy('links.id')
            ->get(array(
                'events.*',
                'competitions.name as competitions_name',
                'channels.id as channel_id',
                'channels.title as channel_title',
                'links.id as link_id',
                'links.language as link_language'
            ));
        return $events;
    }

    /**
     * Get Events without channels by User (panel)
     *
     * @param integer $user_id The User Id
     * @param string $tz The Timezone
     * @return mixed
     */
    public static function getEventsWithoutChannels($user_id, $tz = 'Europe/Madrid')
    {
        //TODO: Expire cache. ->remember(5)
        $today = Carbon::today('UTC');
        $tomorrow = Carbon::tomorrow('UTC');
        $tomorrow->addDays(4);

        $sports = Sport::select('id')->lists('id');
        $events = Event::leftJoin('competitions', function ($join) {
            $join->on('competitions.id', '=', 'events.competition_id');
        })
            ->leftJoin('sports', function ($join) {
                $join->on('sports.id', '=', 'competitions.sport_id');
            })
            ->where('events.status', '=', 1)
            ->where('published_at', '>', $today)
            ->where('published_at', '<', $tomorrow)
            ->whereIn('competitions.sport_id', $sports)
            ->orderBy('events.published_at', 'ASC')
            ->get(array(
                'events.*',
                'competitions.name as competition_name',
                'sports.slug as sport'
            ));
        return $events;
    }

    /**
     * Get Events for Admin (admin)
     */
    public static function getActiveEvents()
    {
        $events = Event::leftJoin('competitions', function ($join) {
            $join->on('competitions.id', '=', 'events.competition_id');
        })
            ->where('events.status', '=', 1)
            ->orderBy('events.published_at', 'ASC')
            ->get(array(
                'events.*',
                'competitions.name as competition_name',
                'competitions.slug as competition_slug'
            ));
        return $events;
    }

}