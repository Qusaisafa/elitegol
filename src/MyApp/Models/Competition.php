<?php
namespace MyApp\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Competition extends Model
{
    protected $fillable = array(
        'id',
        'name',
        'slug',
        'description',
        'sport_id',
        'status'
    );

    public $timestamps = false;

    public static function isValidate()
    {
        return true;
    }

    public static function getCompetitionsBySportId($id)
    {
        return Competition::where('sport_id', '=', $id)
            ->groupBy('competitions.id')
            ->get();
    }
}