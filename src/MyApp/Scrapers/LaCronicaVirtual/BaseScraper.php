<?php
namespace MyApp\Scrapers\LaCronicaVirtual;

use Carbon\Carbon;
use MyApp\Models\Competition;

class BaseScraper
{
    public $ip_host = '134.0.9.54';
    public $user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36";
    public $competition_data;
    public $sport_id = null;
    public $default_competition = "Amistoso";

    public function process()
    {
        $data = $this->getData();
        $this->setCompetitionData();
        $nodeValues = $this->formatData($data);
        return $nodeValues;
    }

    public function setCompetitionData()
    {
        $data = array();

        if (!$this->sport_id){
            throw new Exception("Error Processing Sport ID", 1);
        }

        $collection = Competition::select('id', 'name', 'alias')
            ->where('sport_id', '=', $this->sport_id)
            ->get();

        foreach ($collection as $item) {
            $data[$item->alias] = array(
                'id' => $item->id,
                'name' => $item->name
            );
        }
        $this->competition_data = $data;
    }

    public function _get_competition($name)
    {
        $competition = isset($this->competition_data[$name]) ?
                $this->competition_data[$name] : null;

        //print_r($this->competition_data);
        //die();

        if (!$competition) {
            return $this->competition_data[$this->default_competition];
        }

        return $competition;
    }

    public static function convert_to_datetime($my_date, $my_time, $event_duration)
    {
        $data = preg_split('/\s+/', $my_date);
        $month = BaseScraper::month_to_number($data[3]);
        $day = $data[1];
        $time = self::get_my_time($my_time);

        $published_at = Carbon::create(2017, $month, $day);
        $published_at->hour($time['hour']);
        $published_at->minute($time['minute']);
        $published_at->second(0);

        $finished_at = Carbon::create(2017, $month, $day);
        $finished_at->hour($time['hour']);
        $finished_at->minute($time['minute']);
        $published_at->second(0);
        $finished_at->addHours($event_duration);

        $result = array();
        $result['published_at'] = $published_at->toDateTimeString();
        $result['finished_at'] = $finished_at->toDateTimeString();

        return $result;
    }
    

    public static function get_my_time($my_time)
    {
        $time = explode('.', $my_time);
        $data = array();
        $data['hour'] = $time[0];
        $data['minute'] = $time[1];
        return $data;
    }

    private static function month_to_number($name)
    {
        $name = strtolower($name);
        $name = rtrim($name, ",");
        $month = array(
            'enero' => 1,
            'febrero' => 2,
            'marzo' => 3,
            'abril' => 4,
            'mayo' => 5,
            'junio' => 6,
            'julio' => 7,
            'agosto' => 8,
            'septiembre' => 9,
            'octubre' => 10,
            'noviembre' => 11,
            'diciembre' => 12,
        );
        return $month[$name];
    }

    function url_slug($str, $replace = array(), $delimiter = '-', $maxLength = 200)
    {

        if (!empty($replace)) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("%[^-/+|\w ]%", '', $clean);
        $clean = strtolower(trim(substr($clean, 0, $maxLength), '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
    

}