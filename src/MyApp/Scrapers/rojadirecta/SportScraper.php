<?php
namespace MyApp\Scrapers\rojadirecta;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class SportScraper extends BaseScraper
{
    private $url = 'http://www.rojadirecta.me/all';
    private $event_duration = 2; # hours
    public $sport_id = 1;
    public $default_competition = "Amistoso";

    public function getData()
    {
//		$client = new Client(array(
//			'HTTP_HOST' => $this->ip_host,
//			'HTTP_USER_AGENT' => $this->user_agent
//		));
        $client = new Client();
        $crawler = $client->request('GET', $this->url);

        $EventlistNodeValues = $crawler->filterXPath('//span[@class="list"]//div[@class="menutitle"]')->each(function (Crawler $node, $i) {
            if($i > 100 ){return false;}

            $event['date'] = $node->filterXPath('//div//../comment()[2]')->each(function (Crawler $node, $i) {
                return $node->text();
            });

            $event['time'] = $node->filterXPath('//div[@class="menutitle"]//span[@class="t"][1]')->each(function (Crawler $node, $i) {
                return $node->text();
            });

            $event['slug'] = $node->filterXPath('//div[@class="menutitle"]//b')->each(function (Crawler $node, $i) {
                return $node->text();
            });

//            $event['compatition_name'] = $node->filterXPath('//div[@class="menutitle"]//b')->each(function (Crawler $node, $i) {
//                return $node->text();
//            });
            $event['compatition_type'] = $node->filterXPath('//div[@class="menutitle"]')->each(function (Crawler $node, $i) {
                return $node->text();
            });



                if(empty($event['slug']))
                    return null;
                $event['time'] = implode("", $event['time']);

                $event['date'] = implode("", $event['date']);

                $event['slug'] = implode("", $event['slug']);

//                $event['compatition_name'] = implode("", $event['compatition_name']);

                $event['compatition_type'] = implode("", $event['compatition_type']);

//                split name from dispaly event title
                $split_next[1] = 'Amisto';
                $split =  explode(':',$event['compatition_type'],3);
                $split_next = explode(' ',$split[1],2);
                $event_name = $split_next[1];
                $event['compatition_name'] = $event_name;

                $event['slug']= self::string_refinement($event['slug']);
                $event['compatition_name']= self::string_refinement($event['compatition_name']);

            return json_encode($event,true);



        });

        return $EventlistNodeValues;
    }

    public function formatData($_data)
    {
        $index = 0;
        if(isset($_data)) {
            $data = array();
            foreach ($_data as $item) {
                $event = json_decode($item, true);
                    if (isset($event)) {
                        $formal_date_time = self::formal_date_time_format($event['time'],$event['date'], 2);
                        $obj = new \stdClass();
                        $obj->title = $event['slug'];
                        $obj->slug = $this->url_slug($event['slug']);
                        $obj->competition_id = '1';
                        $obj->competition_name = $event['compatition_name'];
                        $obj->description = '';
                        $obj->published_at = $formal_date_time['published_at'];
                        $obj->finished_at = $formal_date_time['finished_at'];
                        $obj->type = $event['compatition_type'];
                        $data[] = $obj;

                    }

            }

            return $data;
        }
        return null;
    }

}

?>