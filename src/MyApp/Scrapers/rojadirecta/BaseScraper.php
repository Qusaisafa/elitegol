<?php
namespace MyApp\Scrapers\rojadirecta;

use Carbon\Carbon;
use MyApp\Models\Competition;

class BaseScraper
{
    public $ip_host = '134.0.9.54';
    public $user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36";
    public $competition_data;
    public $sport_id = null;
    public $default_competition = "Amistoso";

    public function process()
    {
        $data = $this->getData();
        $nodeValues = $this->formatData($data);
        return $nodeValues;
        set_time_limit(300);
    }


    public static function convert_to_datetime($my_date, $my_time, $event_duration)
    {
        $data = preg_split('/\s+/', $my_date);
        $month = BaseScraper::month_to_number($data[3]);
        $day = $data[1];
        $time = self::get_my_time($my_time);

        $published_at = Carbon::create(2017, $month, $day);
        $published_at->hour($time['hour']);
        $published_at->minute($time['minute']);
        $published_at->second(0);

        $finished_at = Carbon::create(2017, $month, $day);
        $finished_at->hour($time['hour']);
        $finished_at->minute($time['minute']);
        $published_at->second(0);
        $finished_at->addHours($event_duration);

        $result = array();
        $result['published_at'] = $published_at->toDateTimeString();
        $result['finished_at'] = $finished_at->toDateTimeString();

        return $result;
    }

    public static function formal_date_time_format($time,$date, $event_duration)
    {
                $time = self::get_my_time_with_two_dots($time);

                $year_month_day = explode('-', $date);
                if (isset($year_month_day[1]) and isset($year_month_day[2])) {
                    $month = $year_month_day[1];
                    $day = $year_month_day[2];
                } else {
                    $month = null;
                    $day = null;
                }

            $published_at = Carbon::create(2017, $month, $day);
            $published_at->hour($time['hour']);
            $published_at->minute($time['minute']);
            $published_at->second(0);

            $finished_at = Carbon::create(2017, $month, $day);
            $finished_at->hour($time['hour']);
            $finished_at->minute($time['minute']);
            $published_at->second(0);
            $finished_at->addHours($event_duration);

            $result = array();
            $result['published_at'] = $published_at->toDateTimeString();
            $result['finished_at'] = $finished_at->toDateTimeString();
            return $result;
    }

    public static function get_my_time_with_two_dots($my_time)
    {
        $time = explode(':', $my_time);
        $data = array();
        if (isset($time[0]) and isset($time[1])) {
            $data['hour'] = $time[0];
            $data['minute'] = $time[1];
        }
        else {
            $data['hour'] = "00";
            $data['minute'] = "00";
        }
        return $data;
    }

    public static function get_my_time($my_time)
    {
        $time = explode('.', $my_time);
        $data = array();
        $data['hour'] = $time[0];
        $data['minute'] = $time[1];
        return $data;
    }

    private static function month_to_number($name)
    {
        $name = strtolower($name);
        $name = rtrim($name, ",");
        $month = array(
            'enero' => 1,
            'febrero' => 2,
            'marzo' => 3,
            'abril' => 4,
            'mayo' => 5,
            'junio' => 6,
            'julio' => 7,
            'agosto' => 8,
            'septiembre' => 9,
            'octubre' => 10,
            'noviembre' => 11,
            'diciembre' => 12,
        );
        return $month[$name];
    }

    function url_slug($str, $replace = array(), $delimiter = '-', $maxLength = 200)
    {

        if (!empty($replace)) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//IGNORE', $str);
        $clean = preg_replace("%[^-/+|\w ]%", '', $clean);
        $clean = strtolower(trim(substr($clean, 0, $maxLength), '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

    public static function baloncesto_sport($str){
        if (strpos($str, 'Baseball') or strpos($str, 'baseball') )
            return true;
        else
            return false;
    }

    public static function ciclismo_sport($str){
        if (strpos($str, 'ciclismo') or strpos($str, 'Ciclismo') )
            return true;
        else
            return false;
    }

    public static function tenis_sport($str){
        if (strpos($str, 'Tenis') or strpos($str, 'tenis') )
            return true;
        else
            return false;
    }
    public static function motor_sport($str){
        if (strpos($str, 'motor') or strpos($str, 'Motor') )
            return true;
        else
            return false;
    }

    public static function golf_sport($str){
        if (strpos($str, 'golf') or strpos($str, 'Golf') )
            return true;
        else
            return false;
    }

    public static function deportes_sport($str){
        if (strpos($str, 'Rugby') or strpos($str, 'rugby') )
            return true;
        else
            return false;
    }

    public static function string_refinement($str){
        if(isset($str)) {
            $text = str_replace(array('(', ')', ':', '(-)', '/'), ' ', $str);
            return $text;
        }else
            return null;
    }

    public static function get_sport_id($event_name){
        if(BaseScraper::baloncesto_sport($event_name))
            return 2;
        else if(BaseScraper::tenis_sport($event_name))
            return 4;
        else if(BaseScraper::golf_sport($event_name))
            return 7;
        else if(BaseScraper::deportes_sport($event_name))
            return 8;
        else if(BaseScraper::motor_sport($event_name))
            return 3;
        else if(BaseScraper::ciclismo_sport($event_name))
            return 5;
        return 1;
    }
}