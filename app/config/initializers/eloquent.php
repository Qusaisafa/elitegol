<?php

use \Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Cache\CacheManager as CacheManager;
use \Illuminate\Filesystem\Filesystem as Filesystem;

$database = require __DIR__ . '/../database.php';

$capsule = new Capsule;
$capsule->addConnection($database);

$container = $capsule->getContainer();
$container['config']['cache.driver'] = 'file';
$container['config']['cache.path'] = CACHE_PATH . '/eloquent';
$container['config']['cache.connection'] = null;
$container['files'] = new Filesystem();

$cacheManager = new CacheManager($container);

$capsule->setCacheManager($cacheManager);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$app->db = $capsule;

$connection = $app->db->connection();
$connection->disableQueryLog();
