# Elitegol Clone

## Dependencias

`sudo apt-get install php5-cli php5-intl php5-curl -y`

## Instalar composer

```
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
```

En la carpeta de la aplicación, ejecutas:

`composer install`

## Instalación

Editar los datos de conexión a la base de datos en `app/config/database.php`.

### Migrador

Ejecutar el migrador de tablas. (La herramienta de línea de comandos creará las tablas)

`bin/phpmig migrate`

### Data

Crear la data inicial, ejecutando el script en la consola.

`php db/seed.php`

### Administración

Usuario: `admin@elitegol.eu`

Password: `admin123admin`

### Dev Docs

El framework que se utiliza es SLim.
http://www.slimframework.com/

Sin embargo, se ha tratado de adaptar el patrón MVC.
Por lo que si has usado Laravel o algún framework MVC, es muy sencillo.

#### Routing

Debes tener en cuenta que las routes (URLs) se crean en app/routes.php

En donde se vincula el path (ruta) con una Clase::metodo, estos ficheros estan
en src/MyApp/Controllers

#### Model

El ORM utlizado es Eloquent http://laravel.com/docs/4.2/eloquent
La conexión e integranción ya esta preparada en app/config/initializers

Para crear nuevas clases (modelos=tablas) y nuevos métodos (consultas sql) debes
editar los ficheros en src/MyApp/Models

#### View
Se utiliza Twig para las vistas.

#### Controllers
Vuelve a ver el sistema de Routing.

#### Auth
Sentry para la Autentificación
https://cartalyst.com/manual/sentry/2.1

#### Scraper

Los ficheros que están en app/config/import
contienen las instrucciones para scrapear el contenido de La Cronica Virtual.

Ejemplo: futbol.php

Matching
La página de La Cronica Virtual (futbol.php) devuelve un array. En ese array se
escribe las competiciones exactas. Tal como aparecen en la web de La Cronica.

Si se encuentra, se indentifica la categoria de la WebApp, para que luego tome
esos valores y se inserten en la BD.

Nota: Se podría refactorizar está parte, utilizando alias de categorias.


## CRON Tasks (Ping)

Para que los eventos finalicen y no se muestre en portada, tienes 2 opciones:

* Manual. Acceder desde el Admin y en Eventos, presionar el botón de "Borrar eventos finalizados".

* Automático. El CRON o servicio de PING necesita apuntar a la URL `/cron/tasks/scheduled`

Cada cierto tiempo (Entre 5 o 15 minutos, a elección)


